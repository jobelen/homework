package com.hc.hw.service;

import java.util.List;

import com.hc.hw.model.WeatherLog;

public interface WeatherInfoService {

	List<WeatherLog> getForcast();

    void saveWeatherLogs(List<WeatherLog> list);

}

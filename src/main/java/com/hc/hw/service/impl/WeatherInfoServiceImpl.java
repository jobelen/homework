package com.hc.hw.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hc.hw.model.WeatherLog;
import com.hc.hw.model.dto.ResultDTO;
import com.hc.hw.repository.WeatherLogRepository;
import com.hc.hw.service.WeatherInfoService;

@Service
public class WeatherInfoServiceImpl implements WeatherInfoService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    WeatherLogRepository repo;

    @Value("${weathermap.url}")
    String url;

    @Value("${weathermap.appid}")
    String appid;

    @Value("${weathermap.loc}")
    String loc;

    @Override
    public List<WeatherLog> getForcast() {

        ResultDTO result = restTemplate.getForObject(url, ResultDTO.class, loc, appid);

        List<WeatherLog> toSave = new ArrayList<>();

        result.getList().forEach(weather -> {
            WeatherLog itm = new WeatherLog();
            itm.setActualWeather(weather.getWeather().get(0).getMain());
            itm.setDtimeInserted(new Date());
            itm.setLocation(weather.getName());
            itm.setTemperature(weather.getMain().getTemp().toString());
            itm.setResponseId(UUID.nameUUIDFromBytes(itm.toString().getBytes()).toString());

            toSave.add(itm);
        });

        saveWeatherLogs(toSave);

        return toSave;
    }

    @Override
    public void saveWeatherLogs(List<WeatherLog> list) {

        // check for uniqueness and delete first record if records became greater than 5
        list.forEach(w -> {
            List<WeatherLog> rs = repo.findByLocationAndActualWeatherAndTemperature(w.getLocation(), w.getActualWeather(), w.getTemperature());
            if (rs.isEmpty()) {
                repo.save(w);
                if (repo.count() > 5) {
                    repo.delete(repo.findFirstByOrderByIdAsc());
                }
            }
        });

    }

}

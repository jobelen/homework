package com.hc.hw.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hc.hw.model.WeatherLog;

@Repository
public interface WeatherLogRepository extends JpaRepository<WeatherLog, Long> {
    
   List<WeatherLog> findByLocationAndActualWeatherAndTemperature(String loc, String weather, String temp);
    
   WeatherLog findFirstByOrderByIdAsc();

}

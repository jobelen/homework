package com.hc.hw.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hc.hw.model.WeatherLog;
import com.hc.hw.model.dto.ResultDTO;
import com.hc.hw.service.WeatherInfoService;

@RestController
public class WeatherInfoController {

	@Autowired
	WeatherInfoService weatherInfoService;
	
	@RequestMapping("/")
	public List<WeatherLog> welcome() {
		return weatherInfoService.getForcast();
	}

}

package com.hc.hw.model.dto;

public class WindDTO {
	
    private Long speed;
    private Long deg;
	
	public Long getSpeed() {
		return speed;
	}
	public void setSpeed(Long speed) {
		this.speed = speed;
	}
	public Long getDeg() {
		return deg;
	}
	public void setDeg(Long deg) {
		this.deg = deg;
	}
	
	

}

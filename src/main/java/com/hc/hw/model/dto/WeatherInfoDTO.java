package com.hc.hw.model.dto;

import java.util.List;

public class WeatherInfoDTO {

    private Long id;
    private Long visibility;
    private String name;
    private CoordinatesDTO coord;
    private MainDetailsDTO main;
    private WindDTO wind;
    private CloudsDTO clouds;
    private String dt;
    private SysDTO sys;
	
	List<CityInfoDTO> weather;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVisibility() {
        return visibility;
    }

    public void setVisibility(Long visibility) {
        this.visibility = visibility;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CoordinatesDTO getCoord() {
        return coord;
    }

    public void setCoord(CoordinatesDTO coord) {
        this.coord = coord;
    }

    public MainDetailsDTO getMain() {
        return main;
    }

    public void setMain(MainDetailsDTO main) {
        this.main = main;
    }

    public WindDTO getWind() {
        return wind;
    }

    public void setWind(WindDTO wind) {
        this.wind = wind;
    }

    public CloudsDTO getClouds() {
        return clouds;
    }

    public void setClouds(CloudsDTO clouds) {
        this.clouds = clouds;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public SysDTO getSys() {
        return sys;
    }

    public void setSys(SysDTO sys) {
        this.sys = sys;
    }

    public List<CityInfoDTO> getWeather() {
        return weather;
    }

    public void setWeather(List<CityInfoDTO> weather) {
        this.weather = weather;
    }



}

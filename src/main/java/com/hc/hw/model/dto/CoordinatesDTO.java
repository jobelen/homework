package com.hc.hw.model.dto;

public class CoordinatesDTO {

    private Long lon;
    private Long lat;
	
	public Long getLon() {
		return lon;
	}
	public void setLon(Long lon) {
		this.lon = lon;
	}
	public Long getLat() {
		return lat;
	}
	public void setLat(Long lat) {
		this.lat = lat;
	}
	
	
	
}

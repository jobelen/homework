package com.hc.hw.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MainDetailsDTO {

    private Long temp;
    private Long pressure;
    private Long humidity;

    @JsonProperty("temp_min")
    Long tempMin;

    @JsonProperty("temp_max")
    Long tempMax;

    public Long getTemp() {
        return temp;
    }

    public void setTemp(Long temp) {
        this.temp = temp;
    }

    public Long getPressure() {
        return pressure;
    }

    public void setPressure(Long pressure) {
        this.pressure = pressure;
    }

    public Long getHumidity() {
        return humidity;
    }

    public void setHumidity(Long humidity) {
        this.humidity = humidity;
    }

    public Long getTempMin() {
        return tempMin;
    }

    public void setTempMin(Long tempMin) {
        this.tempMin = tempMin;
    }

    public Long getTempMax() {
        return tempMax;
    }

    public void setTempMax(Long tempMax) {
        this.tempMax = tempMax;
    }

}

package com.hc.hw.model.dto;

import java.util.List;

public class ResultDTO {
    
    private Long cnt;
    private List<WeatherInfoDTO> list;
    
    public Long getCnt() {
        return cnt;
    }
    public void setCnt(Long cnt) {
        this.cnt = cnt;
    }
    public List<WeatherInfoDTO> getList() {
        return list;
    }
    public void setList(List<WeatherInfoDTO> list) {
        this.list = list;
    }

    
    
}
